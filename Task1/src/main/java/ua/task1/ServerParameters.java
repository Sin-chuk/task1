package ua.task1;

public interface ServerParameters {

		String getCommand(String parameter);

		String[] getParametrAndCommand(String parameter);
}
