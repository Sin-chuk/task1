package ua.task1;

import java.util.Arrays;

public class ScannerParam {

	public static void main(String[] args) {

		ServerParameters serverParametersStore = ServerParametersStore.getInstance(args);
		System.out.println();
		System.out.println("Get command:");
		System.out.println(serverParametersStore.getCommand("server"));
		System.out.println(serverParametersStore.getCommand("endpoint"));
		System.out.println(serverParametersStore.getCommand("user"));
		System.out.println(serverParametersStore.getCommand("password"));
		System.out.println(serverParametersStore.getCommand("format"));

		System.out.println();
		System.out.println("Get parametrs and commands:");
		System.out.println(Arrays.toString(serverParametersStore.getParametrAndCommand("server")));
		System.out.println(Arrays.toString(serverParametersStore.getParametrAndCommand("endpoint")));
		System.out.println(Arrays.toString(serverParametersStore.getParametrAndCommand("user")));
		System.out.println(Arrays.toString(serverParametersStore.getParametrAndCommand("password")));
		System.out.println(Arrays.toString(serverParametersStore.getParametrAndCommand("format")));
		System.out.println();
	}
}