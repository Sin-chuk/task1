package ua.task1;

public class ServerParametersStore implements ServerParameters{
	private static ServerParametersStore instance;
	private String[] tempParameters = new String[2];
	private final String DELIMETER = "=";

	public static ServerParametersStore getInstance(String[] parametersArgs) {
		if (instance == null)
			instance = new ServerParametersStore(parametersArgs);
		return instance;
	}

	private ServerParametersStore(String[] parametersArgs) {
		String[] tempParameters1 = new String[2];
		for (int k = 0; k < parametersArgs.length; k++) {
			
			tempParameters1 = parametersArgs[k].split(DELIMETER);

			if (tempParameters1[0].equalsIgnoreCase("server")) {
				setServerParameter(tempParameters1[0]);
				setServerCommand(tempParameters1[1]);
				continue;
			}

			else if (tempParameters1[0].equalsIgnoreCase("endpoint")) {
				setEndPointParameter(tempParameters1[0]);
				setEndPointCommand(tempParameters1[1]);
				continue;
			}

			else if (tempParameters1[0].equalsIgnoreCase("user")) {
				setUserParameter(tempParameters1[0]);
				setUserCommand(tempParameters1[1]);
				continue;
			}

			else if (tempParameters1[0].equalsIgnoreCase("password")) {
				setPasswordParameter(tempParameters1[0]);
				setPasswordCommand(tempParameters1[1]);
				continue;
			}

			else if (tempParameters1[0].equalsIgnoreCase("format")) {
				setFormatParameter(tempParameters1[0]);
				setFormatCommand(tempParameters1[1]);
			} else
				break;
		}
	}

	private String serverParameter, endPointParameter, userParameter, passwordParameter, formatParameter;
	private String serverCommand, endPointCommand, userCommand, passwordCommand, formatCommand;

	public String getServerParameter() {
		return serverParameter;
	}

	public void setServerParameter(String serverParametr) {
		this.serverParameter = serverParametr;
	}

	public String getEndPointParameter() {
		return endPointParameter;
	}

	public void setEndPointParameter(String endPointParametr) {
		this.endPointParameter = endPointParametr;
	}

	public String getUserParameter() {
		return userParameter;
	}

	public void setUserParameter(String userParametr) {
		this.userParameter = userParametr;
	}

	public String getPasswordParameter() {
		return passwordParameter;
	}

	public void setPasswordParameter(String passwordParametr) {
		this.passwordParameter = passwordParametr;
	}

	public String getFormatParameter() {
		return formatParameter;
	}

	public void setFormatParameter(String formatParametr) {
		this.formatParameter = formatParametr;
	}

	public String getServerCommand() {
		return serverCommand;
	}

	public void setServerCommand(String serverComand) {
		this.serverCommand = serverComand;
	}

	public String getEndPointCommand() {
		return endPointCommand;
	}

	public void setEndPointCommand(String endPointComand) {
		this.endPointCommand = endPointComand;
	}

	public String getUserCommand() {
		return userCommand;
	}

	public void setUserCommand(String userComand) {
		this.userCommand = userComand;
	}

	public String getPasswordCommand() {
		return passwordCommand;
	}

	public void setPasswordCommand(String passwordComand) {
		this.passwordCommand = passwordComand;
	}

	public String getFormatCommand() {
		return formatCommand;
	}

	public void setFormatCommand(String formatComand) {
		this.formatCommand = formatComand;
	}

	@Override
	public String[] getParametrAndCommand(String parameter) {
		String temp = parameter.toLowerCase();
		
		switch (temp) {
		case "server":
			tempParameters[0] = getServerParameter();
			tempParameters[1] = getServerCommand();
			break;
		case "endpoint":
			tempParameters[0] = getEndPointParameter();
			tempParameters[1] = getEndPointCommand();
			break;
		case "user":
			tempParameters[0] = getUserParameter();
			tempParameters[1] = getUserCommand();
			break;
		case "password":
			tempParameters[0] = getPasswordParameter();
			tempParameters[1] = getPasswordCommand();
			break;
		case "format":
			tempParameters[0] = getFormatParameter();
			tempParameters[1] = getFormatCommand();
			break;
			default:{
				String []errorString = {"ERROR ", "PARAMETER"};
			}

		}
		return tempParameters;
	}

	@Override
	public String getCommand(String parameter) {

		if (parameter.toLowerCase().equalsIgnoreCase("server")) {
			return getServerCommand();
		}

		else if (parameter.toLowerCase().equalsIgnoreCase("endpoint")) {
			return getEndPointCommand();
		}

		else if (parameter.toLowerCase().equalsIgnoreCase("user")) {
			return getUserCommand();
		}

		else if (parameter.toLowerCase().equalsIgnoreCase("password")) {
			return getPasswordCommand();
		}

		else if (parameter.toLowerCase().equalsIgnoreCase("format")) {
			return getFormatCommand();
		}

		else
			return "Command not found";
	}
}
